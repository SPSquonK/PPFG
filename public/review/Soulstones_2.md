# Pokemon Soulstones 2 - Time Wardens

*Review written during v1.0.4 / v1.0.6 / v1.0.7*

## General

Still impressed by Jos Luis productivity. Soulstones is like 2 years old, and here is another very long game with lots of new places, lots of new Pokemons, that all have a full moveset ... Sure some species are shared with Soultones 1, but there are still like 500 new species.

It is easy to see the improvements over Soulstones: way more custom sprites, no more "you are only fighting the good people" issue that Soulstones main story had, a way more controlled levelling ...

However, overall, I felt like the game was less fun than Soulstones because of three aspects:
- The type chart has been changed a lot, and there are only regionals Pokemons. It means that basically you are lost when playing the game as you don't know anything about what is good or bad
- The game pushes a lot this competitive feeling that you should play perfectly (through the AP system, Battle Belt, challenging fights even with the lowest difficulty setting, very constrained levels) instead of letting the player just being there for the ride.
- It's yet another fan game that focuses on Arceus. It is so overly done that Arceus itself is bored with being one of the focus of the game. The dude does not want to get involved, ##LeaveArceusAlone.
  - Ok, I recognize that the fact that Arceus does not care about the plot is unique to this game. But Dialga/Palkia/Giratina are overly concerned.


The really interesting aspect of the story is that we learn more about the Time Wraith purpose and learn about the anomalies. The anomalies subplot is way more interesting than the "some people made war for some dumb reasons" plot, but it seems that it is reserved for post game.


Play time = 25h33 when saving, 31h in Hall of Fame (don't know why the time is different), 663 caught species (I just caught everything, but did not bother evolving)
  - I probably played less because several times (2 or 3 times?), I left my computer for like 1 or 2 hours without closing the game

Team:
- Before Telescope: Eevee/Slowpoke/Mankey/T.Hoothoot/Phanpy/Filler
- Before Inbetween: Annihilape/Donphan/Noivern/Lunatone/Dragapult/Druddigon
- Since Inbetween: T.Magnezone replaced Noivern

Mostly played on 1.0.4. Switched to 1.0.6 late on the game.

## Menu ergonomy

- When starting the game, the game explains the difficulty settings and the fact that Battle belt is enabled on some of them. Please explain what "Battle belt" is because I had no idea.
- The game should memorize I am on the Pokeball tab.
  - Instead, currently I have to tap right each time I open the Item menu.
- A key press should move the menu one step from the left or the right. Instead, currently, as long as you are holding left, the menu is moving. It means that if you are playing with speed up, it is basically impossible to select the menu option just on the left or just on the right.

- While the M and N options in battle are a good idea, maybe the types of the Pokemons and the effectiveness of the moves should be displayed in the base UI.
  - I've seen another fan game that directly displays the type on the UI: Pokemon Anil. Maybe you could use it as an inspiration.
  - For type effectiveness, just do something similar as Pokemon Scarlet/Violet
  - Flying press resists is not right on M (it does not take into account the flying component)

- The Escape key should open the menu. (like shift)

- Mushroom trader should display how many mushrooms I have at Indus City. Similar remark with similar NPC, because if the amount of items I have is not displayed, I am just going to select all options, get trashtalked by the NPC because I don't have any, then forget the NPC.

- In Options, On and Off are not always located at the same position: sometimes On is on the left, sometimes it is on the right

## Leveling

- The exp boost/malus is a good idea to control the levels of the player.

- There are still Blissey trainers and no exp all. Exp all is just a better system, especially as the game quickly requires to have a team of 6. I don't understand why you refuse to give it from the beginning of the game.
  - Sure we have Exp Share, but it only applies to one Pokemon, and swapping Exp Share between the team was annoying, as I had to make sure that Eviolite was still on a relevant Pokemon, remember who has Amulet Coin, remember who has Demonic Eye, ...

## Karma and quests

I had a lot of issues with how karma is managed in this game while playing. However, it turns out Karma has no impact in the main story.

- I don't understand what kind of actions gives me karma or removes it
  - I steal things to the Ethereal guild = the evil guys and I lose karma?
  - Explore places with the need of ~~knowing the Pokedex~~ know how to search in a text file. Lose Karma?
  - As when an NPC gives me a quest, I am unsure if I will earn or lose karma, I stopped doing quests.
- Please adopt the convention of top choice = no karma change or karma boost ; bottom choice = no karma change or karma malus
  - Or top choice = no karma change, other choices = karma change
  - The idea is I talk to everyone and when an NPC starts talking no sense, I am just going to mash Enter to get out of this dialogue.
  - Ok, later in the game, I can just buy Karma boost/malus so I could just have done all quests. However, I have no idea what having a good or bad karma changed. (except some pokemons refused to talk to me)

Overall, quests are very repetitive: "hey there are some guys that are corrupted. Defeat them all". I know most fan games have very repetitive side quests, but in this game it is pretty unreal how repetitive they are. I think Rejuvenation is a good example of good side quests in a Pokemon game.
  - The best quest is probably the hostel quest
  - If the first quest in Europa Lake only had one web to investigate, and if you remove the time requirement, it would also be a good quest as a first quest. 

- The clown in the starting town should not remove karma when talking to them without knowing what they are about. It is very bad as you are telling the player "do not talk to everyone".
  - Talking to an NPC and giving him the 000000 code is not bruteforcing
  - And don't get me started on how much this code is a bad idea to begin with. People do not join the Discord server of all Pokemon fan games they are playing
  - I'm pretty sure I did a similar remark on Soulstones 1.

## The type chart / Epoch pokemons typing

I know this will be controversial but:

- I dislike the new types
  - Light type already exists in the Fairy type
  - Cosmic type makes no sense. Is it a space type?
    - The type chart seems to indicate that you are going to fight the outer space itself, but most Pokemons with the cosmic types are space hobbyist. Look at Epoch Delphox sprite and explain why it should be immune to ground.
    - Notice that the "Flying" type is named "Flying" and not "Bird" or "Wind". This is because no Pokemon but Tornadus is the wind.
  - Sound is an ok typing
- I don't understand the type chart changes, nor the type chart at all
  - I don't understand why Steel does not resist Dragon anymore: Steel used to be THE type to use to resist dragons, especially in gen 5 that was the generations of dragons (in addition to the weather war) where a competitive team had to have some steel type to them.
  - There probably is a double weakness on Anihilape, but I don't know to which type. There are probably weakness shared by too many of my team members (4 or more pokemons sharing the same weakness without any resist) but I don't know what. Because again, I just don't understand the type chart.
- I don't understand the typing of most Pokemons.
  - Here are some examples I took from my Pokedex before Telescope Academy. Gible is Ghost/Dragon: how is it a ghost? Electrike is a ghost: how is it a ghost? Rookidee is dark/electric: dark? Stunky is normal/poison: no dark type even though it looks like Galarian Zigzagoon? Same with Yamper. Metapod is a fairy? Pidove is psychic? Hopip is sound? Cacnea fight? Growlithe is a dragon? Vullaby ground? Buneary ice? Pineco fairy? T Bounsweet poison fight? Trubbish electric? T Slugma psychic? T Surskit flying? T Skitty light? T Numel rock? Munchlax psychic? T Swinub ground? T Budew dragon? Lotad ground? Wattrel rock? Wooper fire? Delibird dark?

- As the typing of Pokemons is so unpredictable, and as I don't understand the type chart, what I did is just press the M key to see the move effectiveness ALL THE TIME. When I did not press M because I was like "ok it is low hp, I can use any move" -> I end up using a sound move on a cosmic Pokemon. Oops, cosmic is immune to sound.

- There are a ton of cool regional designs
  - Politoed, Slowpoke line, Water Slugma, new Shellos/Gastrodon forms, Numel, Ground Totodile line, Rowlet, Fire Espurr, Both Murkrows, Pichu line, Sandile line, Electric Swinub, Bug Horsea line, Omanyte line, Phanpy line, Applin lines, Fennekin line, Ice Numel line, Stand Machop line, Nice Munna evolving into Glitched Musharna, ...
  - But as the game is more challenging than the previous one, and as I was totally lost by the typings and the type chart, my reaction when seeing a Pokemon was "what move do I use? Is this thing viable" instead of "that's a cool design, let's use it".
    - I would have loved to use Slowpoke, Water Slugma and Ground Totodile, but normal type is a too bad type in the early game (for Slowbro)/they were just too weak early game, so I ended up using more efficient Pokemons aka single staged Pokemons (Lunatone and Druddigon) to make the game a bit easier.


## Misc systems

- I am a bit annoyed by how the AP system is presented. It is introduced after the first boss and I already started to catch Pokemons as the NPCs are encouraging me to. The system basically tells me "your save is maybe fucked up because you are not playing on highest difficulty". Then Breloom tells me that the rewards will be listed later. Like I have no idea if I have to play on highest difficulty while playing perfectly, or if I can play very casually.
  - I had several remarks like "13 hours in, still don't know what AP are used for", but I finished the game and I still don't know what APs are used for.

- Demonic eye is cool. It would be probably better as a key item to leverage the weight of doing held item management all the time.

- Camping gear is a good idea
  - It should have a better UI to change the hour. Like for example the one to choose the hour in GSC
  - The issue being if you want to set the time to 07, and it is 21, if you change the 10th digit by going up, you are going to browse 31, 41, 51... o'clock until reaching 91 and then 01.
  - Also display when is day/night/morning, especially when they start.


- Triple Triad:
  - Triad players in mayor tower can be rematched
  - All Triple Triads have weak cards = I have better cards than all my opponents since the beginning of the game. I don't think having NPCs that play Triple Triad in all towns is relevant if they all have very weak cards.

- Move tutors through Pokegear is a good idea but It could be improved further:
  - First list the free tutors then the paid move tutors
  - Display a description of the moves, at least type/power/accuracy. Description would be nice
  - When talking to the npc, don't open the move tutor menu. Just make the NPC says "I know this move. I've added it to your pokegear" because most of the time (never?), I am not going to teach the move.

- Live Support is a good idea on paper, but it should scale with the number of hp or the level. It is useful low level, and quickly it just becomes an item that ensures that you party always starts with everyone alive.
  - Which means that slow pokemons are just going to get killed at each fight after they died once because they will come back with 5 hp, until you properly heal them. But at least, it removes the need to use revives out of battle

- You can restore moves PP by forgetting a move and then relearning it.


## Fights

- I killed Anomaly Marshtomp with Anihilape, I lost some spdef but defiant did not trigger

- I did not use them, but aren't Epoch Munchlax and Epoch Snorlax overpowered? Huge bulk + moody. But maybe their typing is bad, I don't know.

- The mix of 1vs1 and 2vs2 is worse than Soulstones. In Soulstones, basically, you knew that main game was 1vs1, post game was 2vs2. I hate 2vs2 but ok it was consistent. In this game, 2vs2 randomly appears at some places and then disappears. I already don't know what I am doing with my team building, don't add more headaches...


## Random


- The game is very limited money wise. I started to have the money to bump the IV / chose the abilities after the volcano. Before, all my money went to Pokeballs. And by Pokeballs, I really mean pokeballs, not superballs etc.
  - Or maybe, balls just cost too much considering that there are 700 wilds species to catch?


- Performance issue while browsing boxes: https://youtu.be/zPbbk0Wy9Cg
  - If you browse a lot of boxes, the game gets slower and slower for each browsed box.

- Quests descriptions are not consistent: "Caitlin's expecting me" vs "Find 50 Hollow Bones". Are quests written with the first person or the third person?

- If I remember correctly, Soulstones npcs were generous in terms of given items. In this game, I feel like almost none of them give items, and instead are just talking non sense? That's a bit sad.

## Dialogues

I had a lot of issues with dialogues. In general, they are way too long.

(Soulstones 1 also had this problem. But I quickly read the dialogues of Soulstones in my first playthrought, did not read in all subsequent playthrought until my last where I focused on the story.)


- Dialogues are written as if characters were talking. They are making hypotheses, repeating some peace of information, ...
- However, Pokemon text boxes only have 2 lines displayed at once. Long text tends to lose the reader. Especially as a non native english reader, even though I read a lot of english for my work, I quickly get bored if the text does not move on.

Here are some examples of dialogues that annoyed me:
  - Pierre Carleton says twice in a row that he believes that the Ethereal guild infiltrated the protesters.
    - This dialogue do not even answers the question that needs an answer = why are they protesting against leylines thing extraction?
    - The dialogue doesn't even properly tell me what needs to be done = find the Ethereal members and defeat them
  - In Telescope Academy, again, NPCs are saying thrice the same thing.
  - "Anna provides an escort" -> hey that's me, and I didn't read because I was like "that's not me" the line before when the NPC said "Margaux and Paul, you go to New York."
  - The exposition from Ezreal liberation to arriving to Caitlyn's house in the snow city is way too much to handle. I stopped reading at some point because I was like "hey, are they really going to explain to me for 2 hours that Giratina is not that bad, and that the other 3 are actually bad?"

Maybe it would be fine if text boxes were bigger = were able to display more information at once. But in the current state, you mostly just mash Enter to make the game move on, and don't really focus because even when mashing, the dialogues are going to take a long time with a lot of repetitions.


## Notes taken while playing relatively to the plot

- Reusing the same characters as the ones in Soulstones 1 is a good idea. It reduces the amount of characters.

- It is weird to have an NPC in town saying you need to win the Triple Triad tournament to get a boat pass, but you still need to go to the boat guy to learn that "oh no you have to backtrack to play Triple triad" -> I was lost because I did not understand why I could not play the tournament

- "Which form of Honedge would you like?" -> none? let me refuse to take the Pokemon because I don't know what I will be missing.

- Adding burn to the first pokemon of the player is a bad gimmick. Either you know the burn is coming and it is useless, or you don't know and you are in a bad situation.

- Please add at least one healing point in the Temple of Sienna (or buff the Live support item)

- Route 5B, and to a lesser extent the Time Pyramid, level design is bad: it's just some very long pathway, and you are lost because nothing tells where to go
  - Overall I am a bit annoyed by the general level design: there are a lot of junctions, but nothing really tells what is the continuation of the level and what is an optional place. So you end up picking one, and if it is too long, you backtrack to the other one. The issue with Route 5B and Time Pyramid being both the optional paths and the mandatory one are very long
  - Same with the 4th map of InBetween
  - There should probably be a dirt road from point A to point B (because lots of people are passing), and normal grass in optional places, for example in the route to the sky city from the teleporter to the town.
    - See CrossCode level design for example: the game is based on puzzles and enigma. But looking at the ground provides suggestions of what to do so you are never lost.
  - In the desert route, when going to the Pyramids from the most top right city, there is a hole than is there just for frustration.


- Thanks for the summary of the tablets in Cassiopeia Oasis because I totally not read the tablets content itself.

- Caitlyn monologues are fun and short enough to not be annoying.

- I didn't encounter any anomaly in Leo&Selene temple, even though the characters seems to say that I should encounter some.

- Maximilien goes behind the main character after Ezreal's Misdreavus fight (instead of staying in place)

- why are you naming Ezreal "Prisoner" after the fight? You already said that his name is Ezreal

- In Eridamus Settlement, you require me to talk to all cultists, all of them talk a lot, and I need to remember which one I talked to because talking again to an NPC restarts a very long dialogue instead of a 1 sentence summary.
  - From Eridamus Settlement to arriving to Caitlin's house, I stopped caring about the scenario because it was way too much exposition. I got it, Giratina good, Arceus/Palkia/Dialga bad, Etheral/Epoch Corp worse, Time Wraith are dead and it is bad because Giratina was in prison. Let me be free, then I'll get interested again in the plot.

- No surfing Pokemon on Route 13B?

- At some point (between Epoch Politoed first location and the Temple of creation), an NPC tells to an anomaly "You can talk?": please, anomalies are talking since the beginning of the game. Why are you surprised?

- The champion of the time and space rooms of Temple of Creation is not consistent (Koga in the preview, then Shadow guy of BW in battle)

- The "you must find and defeat everyone to progress" trope (especially on routes near Draco City) feels like you are adding some artificial play time. The "find" part is especially annoying as I have to circle a lot in the area to find who I did miss

- The house in Draco city where there was the Pokemon center have different walls positions before and after the battle of Draco city.


## Things

- Rock Shellos and Gastrantrum can only be obtained through Wonder Trade.

- ~~Battle Tower resets you EV if you have more than 510~~ It was fixed in a later version.

- In Battle Factory, I got a Glalie with Assault Vest and Glare/Play Rough/Substitute/Toxic.

- ~~When doing the Battle Tower, a trainer did not send any Pokemon. I'm too lazy to copy the stacktrace, but basically `sum -= 1 if m.prio>0` -> `m.priority`.~~ Was probably fixed

- Breeding is annoying because when you are running back and forth next to the daycare:
  - The old man does not turn change direction when he has an egg to offer (you are forced to talk to him)
  - There are two NPC on the row next to Day care and they go on your way all the time
  - The row is very short

- I wanted to replay the end by choosing the other side, but turns out the choice is a lie. It's a bit disappointing. But I recognize that offering a real choice here would lead to way more work for the post game (reminder that post game in Soulstones means "the other half of the game")


- In route 13B, after fixing the world, there are NPCs moving in square, and you can't block them to talk to them.
