# Rejuvenation 13.5: my script modifications


- Level up moves are less annoying
  - When a Pokemon tries to learn a move, I can just maintain cancel, and it will skip all dialogues until the next move or the levelling is finished. Instead of having to confirm that I don't want to learn lear.

```diff
-        elsif pbDisplayConfirm(_INTL("Should {1} stop learning {2}?",pkmnname,movename))
-          pbDisplayPaused(_INTL("{1} did not learn {2}.",pkmnname,movename))
+        elsif true
```


----------------------

- No nickname
  - I can rename Pokémons from the menu, don't need to be asked all the time.

```diff
-      if pbDisplayConfirm(_INTL("Would you like to give a nickname to {1}?",pokemon.name))
+      if false
```


----------------------

- Always send to the box

```diff
-        if Kernel.pbConfirmMessageSerious(_INTL("The party is full; do you want to send a party member to the PC?"))
+        if false
```

----------------------

- Improve EV/IV debug
  - Faster 252 EV setting
  - 31 IV for Pokemons imported from another save file

```diff
@@ -1219,10 +1219,15 @@ def pbPokemonDebug(origin, pkmn, pkmnid=nil, selected=nil, heldpoke=nil)
                   params.setRange(0,255)
                   params.setDefaultValue(pkmn.ev[cmd2])
                   params.setCancelValue(pkmn.ev[cmd2])
                   f=Kernel.pbMessageChooseNumber(
                      _INTL("Set the EV for {1} (max. 255).",stats[cmd2]),params) { @scene.update }
+                  if f == 1
+                    f = 252
+                  elsif f == 250
+                    f = 0
+                  end
                   pkmn.ev[cmd2]=f
                   pkmn.totalhp
                   pkmn.calcStats
                   @scene.pbHardRefresh
                 end
@@ -1250,16 +1255,16 @@ def pbPokemonDebug(origin, pkmn, pkmnid=nil, selected=nil, heldpoke=nil)
                      _INTL("Set the IV for {1} (max. 31).",stats[cmd2]),params) { @scene.update }
                   pkmn.iv[cmd2]=f
                   pkmn.calcStats
                   @scene.pbHardRefresh
                 elsif cmd2==ivcommands.length-1
-                  pkmn.iv[0]=rand(32)
-                  pkmn.iv[1]=rand(32)
-                  pkmn.iv[2]=rand(32)
-                  pkmn.iv[3]=rand(32)
-                  pkmn.iv[4]=rand(32)
-                  pkmn.iv[5]=rand(32)
+                  pkmn.iv[0]=31
+                  pkmn.iv[1]=31
+                  pkmn.iv[2]=31
+                  pkmn.iv[3]=31
+                  pkmn.iv[4]=31
+                  pkmn.iv[5]=31
                   pkmn.calcStats
                   @scene.pbHardRefresh
                 end
               end
             # Randomise pID
```


----------------------

- Instant egg hatching
  - For Pokemons that are available in the wild, I went in the wild to catch them. If they are unique, I'm not going to run for hours.

```diff
 Events.onStepTaken+=proc {|sender,e|
    next if !$Trainer
    for egg in $Trainer.party
      if egg.eggsteps>0
-       egg.eggsteps-=1
+       egg.eggsteps-=100
        for i in $Trainer.party
          if !i.isEgg? && ((i.ability == :FLAMEBODY) || (i.ability == :MAGMAARMOR) || (i.ability == :STEAMENGINE))
            egg.eggsteps-=1
            break
          end
```

-----------------------

- Don't use my items
  - I wanted to keep my King's Rocks :c

```diff
@@ -670,15 +670,11 @@ class PokemonEvolutionScene
         Graphics.update
       end
       Kernel.pbMessageDisplay(@sprites["msgwindow"],
          _INTL("\\se[]Congratulations! Your {1} evolved into {2}!\\wt[80]",pkmnname,newspeciesname))
       @sprites["msgwindow"].text=""
-      if removeItem
-        @pokemon.item=nil
-        @pokemon.itemInitial=nil
-        @pokemon.itemReallyInitialHonestlyIMeanItThisTime=nil
-      end
+
       $Trainer.pokedex.dexList[@newspecies][:seen?]=true
       $Trainer.pokedex.dexList[@newspecies][:owned?]=true
       $Trainer.pokedex.setFormSeen(@pokemon)
       @pokemon.name=newspeciesname if @pokemon.name==oldspeciesname
       @pokemon.calcStats
```

```diff
@@ -250,11 +250,10 @@ def pbTradeCheckEvolution(pokemon,pokemon2,tradestonecheck=false)
           pokemon.species==:KARRABLAST && !$Trainer.party.any? {|mon| mon.species==:SHELMET}) && pokemon2==:LINKSTONE
           next evo
         end
       when :TradeItem
         if pokemon.item==condition && !tradestonecheck
-          pokemon.setItem(nil)
           next evo
         elsif pokemon.item==condition
           next evo
         end
       when :TradeSpecies
```

---------------------------------

- Always Pickup
  - + Heal at the end of battles, because I could just open the remote PC and store/withdraw Pokemons. It makes the gauntlet easier and some other fights illegitimately, but I don't care.


```diff
@@ -1409,12 +1409,12 @@ def pbCheckAllFainted()
   end
 end
 
 # Runs the Pickup event after a battle if a Pokemon has the ability Pickup.
 def Kernel.pbPickup(pokemon)
-  return if !(pokemon.ability == :PICKUP) || pokemon.isEgg?
-  return if !pokemon.item.nil?
+  pbHealAll
+
   return if rand(10)!=0
   pickupList= PickupNormal
 
   pickupListRare= PickupRare
   return if pickupList.length != 18
@@ -1439,25 +1439,18 @@ def Kernel.pbPickup(pokemon)
   end
 end
 
 # Runs the Pickup event after a battle if a Pokemon has the ability Pickup.
 def Kernel.pbRunPickup(pokemon)
-  return if !(pokemon.ability == :PICKUP) || pokemon.isEgg?
-  return if !pokemon.item.nil?
   item = Kernel.pbPickup(pokemon)
   return if item.nil?
   if $PokemonBag.pbCanStore?(item)
     $PokemonBag.pbStoreItem(item)
   else
     pokemon.setItem(item)
   end
   itemname = getItemName(item)
-  if itemname.start_with?(/[aeiou]/i)
-    Kernel.pbMessage(_INTL("{1} picked up an {2}!", pokemon.name, itemname))
-  else
-    Kernel.pbMessage(_INTL("{1} picked up a {2}!", pokemon.name, itemname))
-  end
 end
```

-----------------------------

- I hate Mining, I love resources

```diff
@@ -242,33 +242,41 @@ class MiningGameScene
     # Set items to be buried (index in ITEMS, x coord, y coord)
     ptotal=0
     for i in ITEMS
       ptotal+=i[1]
     end
-    numitems=2+rand(3)
+    nbiterations = 0
+    numitems=50
     while numitems>0
       rnd=rand(ptotal)
       added=false
       for i in 0...ITEMS.length
         rnd-=ITEMS[i][1]
         if rnd<0
           if pbNoDuplicateItems(ITEMS[i][0])
             while !added
+              nbiterations += 1
+              
               provx=rand(BOARDWIDTH-ITEMS[i][4]+1)
               provy=rand(BOARDHEIGHT-ITEMS[i][5]+1)
               if pbCheckOverlaps(false,provx,provy,ITEMS[i][4],ITEMS[i][5],ITEMS[i][6])
                 @items.push([i,provx,provy])
                 numitems-=1
                 added=true
               end
+
+              break if nbiterations >= 5000
             end
           else
             break
           end
+          break if nbiterations >= 5000
         end
+        break if nbiterations >= 5000
         break if added
       end
+      break if nbiterations >= 5000
     end
     # Draw items on item layer
     layer=@sprites["itemlayer"].bitmap
     for i in @items
       ox=ITEMS[i[0]][2]
@@ -364,32 +372,32 @@ class MiningGameScene
 
   def pbHit
     hittype=0
     position=@sprites["cursor"].position
     if @sprites["cursor"].mode==1   # Hammer
-      pattern=[1,2,1,
-               2,2,2,
-               1,2,1]
+      pattern=[9,9,9,
+               9,9,9,
+               9,9,9]
       @sprites["crack"].hits+=2 if !($DEBUG && Input.press?(Input::CTRL))
     else                            # Pick
-      pattern=[0,1,0,
-               1,2,1,
-               0,1,0]
+      pattern=[9,9,9,
+               9,9,9,
+               9,9,9]
       @sprites["crack"].hits+=1 if !($DEBUG && Input.press?(Input::CTRL))
     end
     if @sprites["tile#{position}"].layer<=pattern[4] && pbIsIronThere?(position)
       @sprites["tile#{position}"].layer-=pattern[4]
       pbSEPlay("MiningIron")
       hittype=2
     else
-      for i in 0..2
+      for i in -BOARDWIDTH..BOARDWIDTH
         ytile=i-1+position/BOARDWIDTH
         next if ytile<0 || ytile>=BOARDHEIGHT
-        for j in 0..2
+        for j in -BOARDWIDTH..BOARDWIDTH*2
           xtile=j-1+position%BOARDWIDTH
           next if xtile<0 || xtile>=BOARDWIDTH
-          @sprites["tile#{xtile+ytile*BOARDWIDTH}"].layer-=pattern[j+i*3]
+          @sprites["tile#{xtile+ytile*BOARDWIDTH}"].layer-=9
         end
       end
       if @sprites["cursor"].mode==1   # Hammer
         pbSEPlay("MiningHammer")
       else
```



-----------------------------


- Give my Darumacho

```diff
@@ -455,11 +455,10 @@ class TilePuzzleScene
   end
 
   def pbCheckWin
     for i in 0...@boardwidth*@boardheight
       return false if @tiles[i]!=i
-      return false if @angles[i]!=0
     end
     return true
   end
 
   def pbMain
```

--------------------

- Please don't kill my SSD

```diff
@@ -13,30 +13,22 @@ module PBDebug
   end
   @@log=[]
   
   def PBDebug.flush
     #if $DEBUG && $INTERNAL && @@log.length>0
-    if $INTERNAL && @@log.length>0
-      File.open("Data/debuglog.txt", "a+b") {|f|
-         f.write("#{@@log}")
-      }
-    end
+    
     @@log.clear 
   end
 
   def PBDebug.log(msg)
     # DEBUG LOGS AUTOMATIC FOR TESTING ONLY, switch which of the next two are commented to change what is being done
     #if $DEBUG && $INTERNAL
     begin
       if File.exist?("Data/debuglog.txt") && File.size("Data/debuglog.txt") > 10_000_000
         File.delete("Data/debuglog.txt")
       end
-      if $INTERNAL
-        File.open("Data/debuglog.txt", "a+b") {|f|
-          f.write("#{msg}\r\n")
-        }
-      end
+      
     rescue
       $INTERNAL = false
     end
   end
```

--------------------

- Sort the Pokemon in non renamed boxes using the dex number.
  - If I was not lazy, I would improve it so it also sort by evolution family (so Tyrogue is before Hitmonlee, and Hitmontop is after Hitmonchan, ...)

```ruby
  def pbSortLiving()
    collected = []

    for i in 0..49
      expected_name = "Box #{i + 1}"

      for poke_slot in 0..30
        poke = $PokemonStorage[i, poke_slot]
        if poke
          collected.push(poke)
          $PokemonStorage[i, poke_slot] = nil
        end
      end
    end

    collected.sort_by! { |poke| (poke.dexnum * 10 + poke.form) * 100 + poke.level }
    # collected.sort_by!{ |poke| poke.timeReceived }

    box = 0
    while $PokemonStorage[box].name != "Box #{box + 1}"
      box = box + 1
    end

    slot = 0

    for poke in collected
      $PokemonStorage[box, slot] = poke

      slot += 1
      if slot == 30        
        box = box + 1
        
        while $PokemonStorage[box].name != "Box #{box + 1}"
          box = box + 1
        end

        slot = 0
      end
    end
  end
```


--------------------

- Box import/export
  - Totally copied from Reborn SWM mods

```ruby
  def pbSaveBox(storage)
    currentBoxId = storage.currentBox
    boxName = "StoredBox-#{currentBoxId}.rxdata"
    
    sFile = RTP.getSaveFileName(boxName)

    if safeExists?(sFile)
      File.delete(sFile)
    end

    File.open(sFile,"wb"){|f|
      Marshal.dump($PokemonStorage[currentBoxId], f)
    }
  end

  def SetBox(x,value)
    if $Trainer
      for i in 0...value.length
        poke = value[i]
        $PokemonStorage[x, i]=poke
        if poke
          if !poke.isEgg?
            $Trainer.pokedex.setOwned(poke)
          end
        end
      end
    end
  end

  def pbLoadBox(storage)
    currentBoxId = storage.currentBox
    boxName = "StoredBox-#{currentBoxId}.rxdata"
    sFile = RTP.getSaveFileName(boxName)

    if safeExists?(sFile)
      File.open(sFile){|f|
        if f.read(2) == Marshal.dump("")[0,2]
          
        else
          Kernel.pbMessage(_INTL("StoredBox is broken"))
        end
      }

      File.open(sFile){|f|
        SetBox(currentBoxId, Marshal.load(f))
      }
    end
  end
```

----------------------------

- Easiest encounters
  - All Pokémons have the same encounter chances
  - No more time dependent encounter

```ruby
class EncounterList
  def initialize()
    @list = []
    @chances = []
  end

  def add(key, y1, y2)
    for element in @list
      if element[0] == key
        element[1] = y1 if y1 < element[1]
        element[2] = y2 if y2 > element[2]
        return
      end
    end

    @list.push([key, y1, y2])
    @chances.push(1)
  end

  def list()
    return @list
  end

  def chances()
    return @chances
  end
end
```

Remove

```ruby
    @enctypes[enctype].each do |key,x| 
      x.each{ |y|
        encounters.push([key,y[1],y[2]])
        chances.push(y[0])
      }
```

Add

```ruby
    encounterList = EncounterList.new()

    if enctype == EncounterTypes::LandMorning || 
      enctype == EncounterTypes::LandDay || 
      enctype == EncounterTypes::LandNight

      for enctype_ in [EncounterTypes::LandMorning, EncounterTypes::LandDay, EncounterTypes::LandNight, EncounterTypes::Land]
        next if @enctypes[enctype_] == nil

        @enctypes[enctype_].each do |key,x| 
          x.each{ |y| encounterList.add(key, y[1], y[2]) }
        end
      end
    else
      @enctypes[enctype].each do |key,x| 
        x.each{ |y| encounterList.add(key, y[1], y[2]) }
      end
    end

    encounters = encounterList.list
    chances    = encounterList.chances
```
