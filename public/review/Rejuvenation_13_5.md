# Pokémon Rejuvenation 13.5

Let's start this review with the mandatory "still a huge work put into this game, offering a lot of playable hours, with no point where the game falls in quality" blah blah blah.

With 13.5, we have 3 major changes:
- Battles are now doable
- Terajuma has been totally changed
- The plot is continued

So I am going to focus this review on these 3 points.


*Team: Typhlosion-Hisui / Ambipom / Porygon2 / Amoonguss / Eelektross / Slowking-Galar / Salazzle / Cloyster / [All the previous guys since Ryland] • Normal difficulty*


------------------


## Battle changes

Don't get me wrong, I was tired of field bullshit in Reborn, I was tired of it in Rejuv V12, was tired of it in Rejuv V13, and I'm still tired of it in Rejuv V13.5. I maintain that if Grookey could override terrain with Grassy Surge, or if there was an option to remove fields, it would provide a way better experience.

### Battles are now doable

Ok now let's be honest: while the "Field_Effect_Manual.txt" file is still totally unreadable, the game provides a basic understanding about the changes made to the moves, specifically the UI shows what is buffed, what is nerfed, and what is getting its type changed. And that's a pretty awesome change. I'm not throwing some moves anymore to an opponent just to discover that I lost a turn.

Moreover, it seems that opponents' difficulty has been turned down. Either that, or being able to see what moves are changed in live by the terrain makes the game way more fair. Every Gaera fight used to be bullshit: it's not anymore. Nym used to be too strong: she is now ok...

I feel like that the only fight that was turned up in terms of difficult is Souta. Psychic gym may also be a bit too hard, but I didn't bother too long with it: I just brought Shedinja + Skill Swap Sturdy Carbink and moved on.


### Except for boss battles

V13 boss mechanic was impossible to get, and I'm glad it was changed to something that is easy to understand.

However, the current difficulty of bosses is unreal. Most of the time, the boss one-shots sweepers, and are usually average to fast. The strategy to defeat bosses are usually:
- Use Fake Out / Priority moves
- Use Toxic
- Use Trick Room

And that's pretty bad because bosses are in this version pretty much a non-relevant aspect of the game: "Oh you are a boss, let me bring by Sallazle to poison you?". The only somewhat legit strategy is using Trick Room, as it enables you to actually fight the boss.

The only really impossible fights are Spacea and Tiempea in bad route. Despite the fact that my bad save file is in Easy Mode ...

Bosses should hit less hard, so we can actually appreciate their respective mechanics. You could totally throw bosses that have 30 hp bars and 255 speed BST. It would still be doable by using some advanced strategies. But you can throw as much mechanics as you want on such boss, it will end up being cheesed and then forgotten.


### Misc

- Xen Mages are a nice gimmick as they tell you what you are doing.
- I'm bothered by the fact that there are no more icons for "BRN", "PSN". Instead, the background changes and I'm never really sure about the status of the Pokémon.

------------------

## Changes

### Terajuma

Ok let's begin with the most important things:
- Melia wears a high ponytail in her charset (the trchar.png file), but on her battle portrait (the Trainer.png file), she has a low ponytail.
- Sometimes, Aelita does not wear her iconic ponytail; especially in Terajuma where she wears unattached long hairs. I think Aelita should keep her ponytail for the whole duration of the game, and at least in Terajuma because 1/ Valarie wears blue long hairs 2/ Venam wears purple twintails. For the whole duration of the Terajuma arc, if you are not fully focused, you are going to confuse Aelita with Valarie. I does not help that Aelita is barely relevant in this story arc, so you are going to forget that one of the two Valaries is actually Aelita.


The remapping of the areas around Kakori village is... ok I guess. I liked the old version, I like the new version. 

Before reaching Teila Resort, the plot feels more messy than the previous iteration.
- Sure, now team Xen is a real menace
- But despite Team Xen saying that they want us dead or alive, we can freely roam in the region, and nobody cares.
- I am seriously unable to remember what we did in what order. We went to Jynobi Pass, Helojak, the shore, the mountain, ... for some reasons... Yeah I don't remember.
  - I played the Terajuma ark until getting the Sky relic during a full afternoon. Then I openned this .md file, and I was unable to remember what I did story wise.
- The two things that are way better in this iteration:
  - No more "I am blocking you from getting the badge for surf to save my friend Saki" aspect from Valarie.
  - The trip to Sheridan factory was weird because you suddenly discovered that you were not actually stuck in Terajuma. Now that it is removed and that the factory has been moved to Helojak it feels more consistent.

- "But to make it harder let's dim the light" NO PLEASE, DON'T DO IT. I barely can see already.
- Not a fan of the changes of Angie's dimension, in particular the interiors. The final map was iconic, and now it is ... random.

### Other

- I liked better the previous Voidal Chasm, even though the new one is simpler
- Ok, so this time there is a really new GDC. I thought in V13 GDC was different than V12 but it turns out that the difference was that gates were gates in V13 but nothing in V12.
  - I still prefer V12 version of GDC even though the V13.5 version has its unique places, and is probably easier to browse when you know the map. 
- Mr Luck tent is nice for move tutor accessibility.
- New Darchlight cave is fun. However, note that when people say "the system recognizes Flora" during Rift Ferrothorn quest, I thought I had to transform into Flora and totally forgot I just had to talk to the door to present the Grass badge.

- Venam not being obnoxious anymore with her DOGGAR song is sad.
- People say that they hate Venam and Erin, but idk, I like them.






------------------

# V13.5 plot

*I enjoyed the good route plot/bonding with the other characters. I liked M2 as a character, and the fun around the bad route. But I feel like the good route should remain/tends more towards being the serious route; and the bad route should just be about destroying the world.*


- I don't understand why you want 13.5 to be skippable in 14. At this game, make the whole game skippable.

- The two routes being interlocked is **bad and unnecessary**:
  - The point is that good MC trades her end of the night spell with bad MC red chain
  - The trade is not needed:
    - Why can't bad MC get the "End of the night" spell by himself?
      - As Good Melia gives him the spell, so can Bad Melia
      - It could just be an Archetype spell of the MC, without any further question. The 4 sisters have spells despite being 1/4 * 1/3 of Arceus. The MC is 1/3 of Arceus, he can have his own spell.
      - In fact, Good Melia being able to build "End of the night" is problematic: how can she produce it? She has shield powers.
    - Why can't good MC get the red chain by herself?
      - She can capture Azelf, Mesprit and Uxie; and both Platinum and PLA shows that you can get the red chain without harming them
      - Spacea and Tiempea don't care that we catch Azelf and pals before defeating them.
  - It raises more questions:
    - How did good MC and bad MC get in touch?
  - It contradicts the player's motivations:
    - If bad MC goal is to destroy everything, he should also want to destroy good MC universe.
    - If good MC goal is to save the world, she should also want to save bad MC universe.
    - Instead, bad MC says "I gain nothing. They gain everything." = he is willing to destroy everything in his world just to produce the red chain, and probably to get rid of Kieran and Claire that will serve as the final boss of Renegade. (Kieran and Claire and probably the same ones in both routes). It defeats the principle of having a bad route: if I am bad with every other characters, I don't want to save them.
  - The red chain origin being a plot point contradicts how the story is told:
    - A lot of major plot points are done off-screen, especially during this chapter: Erin's test, Kieran and MC plotting some things, Ren having to survive...
    - You are supposed to wonder "where does the red chain comes from?". Except you don't wonder it at all when Melia uses it: she has the red chain, ok she has it. Why would I wonder where she got it? Maybe she pulled out a Lin and created the object she needs from nothing.
  - To fully understand the plot, you need to play back to back the good and the bad routes. It kinds of works in V13.5 as playing bad solves the "where does the red chain comes from?" question from good. It probably won't work in later versions as playing subsequent good chapters will make the player forget the question "oh remember the red chain?".
  - Unrelated, but how comes M2 does not know that using the archetype drains her power? In fact, M2 seems to know Venam etc (so it is really Melia), but she doesn't know about things she actually learns in 3rd layer (before the split).



- Bad route is fun, but too mysterious:
  - For most of the game, the MC is silent as it serves as a self insert.
  - There is this thing of "a spell that gives an otherworld being possession of your body", the "otherworld being" being the player. But instead, MC knows things that the players do not.
    - It is a bit annoying during a normal playthrought, but it is for a very short time (Erin enigma) and usually for comedic purposes
    - Here:
      - MC wants to build the red chain, and we do not understand why if we did not play and thought about the good route
      - MC wants to save Ren, and we don't get why
      - There are some hidden plans with Kieran and his girlfriend, but as we spend most of our time with M2, we sympathize way more for M2 cause than Kieran's cause. In fact, at this point, I don't care at all about cooperating with Kieran and Claire, and I would prefer to be able to fully cooperate with M2.
    - Renegade as it is built is a dead end. Renagade saving the world will feel bad because we were promised to destroy the world, not save it. Renegade betraying M2 will feel bad because we get attached to her. Renegade ending up with MC and M2 surviving, living happily and getting a lot of children will feel bad because it is not logical. I can see no good way to finish the story.


- There are way too many interceptors. During V12, interceptor was sold as something very special. As in 13.5, there are at least FIVE interceptors, 3.5 of them being revealed in 13.5 (Crescent is partially revealed in V13 and fully revealed in 13.5). This is bad because from now on, we can assume than everybody and their mothers in V14 will be interceptors. Furthermore, at this point, the 3 new revealed interceptors being interceptors does not add anything to the story
  - V being an interceptor is actually detrimental. A random girl learning about all these things would be way more impactful than "I'm an interceptor lol"
  - Spacea and Tiempa being interceptors is also detrimental. They are Palkia and Dialga, that's enough to give them a role. Furthermore, what are they even intercepting? They are fully part of the system.



------------------

## Misc

- The CG artworks for important scenes are nice

- No player PC for item storage is sad.
  - "Who uses this?" -> me. It is very useful as the game gives you a lot of useful items... that are not that useful. For example, evolution items like Sachet, Silvally's memories, mints, mushrooms, ...


- In V13
  - Still bothered by Ren presence. Still bothered by the fact that Nastasia says that she can't control Ren, although she did in the pyramid.

  - Still bothered by the fact that my character has to fight Melia for the normal badge. I already own one, my Pokémons recognize it as a badge for obedience purpose, and Melia barely has any normal type. I didn't care in v13, I still don't care in v13.5 that some random guys think my badge is legit or not.
    - Talon fight is handled right in this regard. In good route, we are getting close to him. In bad route, he wants to stop us.

